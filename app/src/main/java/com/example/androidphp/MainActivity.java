package com.example.androidphp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ProgressBar loading = findViewById(R.id.progressBar);
        loading.setVisibility(View.GONE);
    }

    public void Entrar(View view){
        dbConnection dbConnection = new dbConnection();
        dbConnection.Login();
    }

    public void TelaRegistro(View view){
        Intent i = new Intent(this, activity_registrar.class);
        startActivity(i);
    }
}
