package com.example.androidphp;

public class Usuario extends Pessoa implements IUsuario {

    String usuario, email, senha;

    public Usuario(String CPF, String Nome, int idade, String usuario, String email, String senha){
        super(CPF, Nome, idade);
        this.usuario = usuario;
        this.senha = senha;
        this.email = email;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
