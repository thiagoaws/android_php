package com.example.androidphp;

public abstract class Pessoa {

    String CPF;
    String nome;
    int idade;

    public Pessoa(String CPF, String nome, int idade){
        this.CPF = CPF;
        this.nome = nome;
        this.idade = idade;
    }
}
