package com.example.androidphp;

public interface IUsuario {
    String getUsuario();
    void setUsuario(String usuario);
    String getEmail();
    void setEmail(String email);
    String getSenha();
    void setSenha(String senha);
}
