package com.example.androidphp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import com.example.androidphp.activity_registrar;

public class dbConnection extends MainActivity implements IdbConnection{
    private EditText txtUsuario, txtSenha, txtRegEmail, txtRegUsuario, txtRegSenha;
    private Button btnEntrar, btnRegistrar;
    private ProgressBar loading;
    private static String URL_LOGIN = "http://54.209.43.179/data/login.php";
    private static String URL_REGIST = "http://54.209.43.179/data/register.php";
    private AlertDialog alerta;

    public void Login(){
        Componentes();
        String sUsuario = txtUsuario.getText().toString();
        String sSenha = txtSenha.getText().toString();

        if(!sUsuario.isEmpty() || !sSenha.isEmpty()){
            Validar(sUsuario, sSenha);
        }else{
            txtUsuario.setError("Usuário inválido!");
            txtSenha.setError("Senha inválida!");
        }

        Validar(sUsuario, sSenha);
    }

    public void Registro(){
        Componentes();
        enviarRegistro();
    }

    public void Componentes(){
        txtUsuario = findViewById(R.id.txtUsuario);
        txtSenha = findViewById(R.id.txtSenha);
        btnEntrar = findViewById(R.id.btnEntrar);
        loading = findViewById(R.id.progressBar);
        txtRegEmail = findViewById(R.id.txtRegEmail);
        txtRegSenha = findViewById(R.id.txtRegSenha);
        txtRegUsuario = findViewById(R.id.txtRegUsuario);
    }

    public void Validar(final String usuario, final String senha){
        loading.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            JSONArray jsonArray = jsonObject.getJSONArray("login");

                            if(success.equals("1")){
                                for (int i = 0; i< jsonArray.length(); i++){
                                    JSONObject object = jsonArray.getJSONObject(i);

                                    String login = object.getString("login").trim();
                                    String senha = object.getString("senha").trim();
                                }
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("login", usuario);
                params.put("senha", senha);
                return params ;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void Validar(final String usuario, final String senha, final String email){
        loading.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            JSONArray jsonArray = jsonObject.getJSONArray("login");

                            if(success.equals("1")){
                                for (int i = 0; i< jsonArray.length(); i++){
                                    JSONObject object = jsonArray.getJSONObject(i);

                                    String login = object.getString("login").trim();
                                    String senha = object.getString("senha").trim();
                                    String email = object.getString("email").trim();

                                }
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("login", usuario);
                params.put("senha", senha);
                params.put("email", email);
                return params ;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void enviarRegistro(){
        loading.setVisibility(View.VISIBLE);

        final String usuario = txtRegUsuario.getText().toString().trim();
        final String senha = txtRegSenha.getText().toString().trim();
        final String email = txtRegEmail.getText().toString().trim();

        StringRequest stringRequest = new StringRequest(StringRequest.Method.POST, URL_REGIST,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");

                            if(success.equals("1")){
                                alertaRegistro();
                                loading.setVisibility(View.GONE);
                                loading.setVisibility(View.VISIBLE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            loading.setVisibility(View.GONE);
                            loading.setVisibility(View.VISIBLE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loading.setVisibility(View.GONE);
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("login", usuario);
                params.put("senha", senha);
                params.put("email", email);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void alertaRegistro(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Cadastrar");
        builder.setMessage("Cadastro feito com sucesso!");
        builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        builder.setIcon(R.drawable.sendimage);
        alerta = builder.create();
        alerta.show();
    }
}
